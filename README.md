# Read Me

Custom docker image created to add Full text search capabilities to the standard mssql image

Original Repo

## Usage

Because this image runs as non root you have have to change the permissions of the mssql data folders

To check if the image is currently running as root exec into the image and run `whoami`

```bash
k exec -it <mssql container id> -- bash
whoami # if out if root run next set of commands

# Update volumes
chown -R 999:999 /var/opt/mssql
chown -R 999:999 /mssql-backup  
chown -R 999:999 /mssql-data  
chown -R 999:999 /mssql-translog
```

### docker-compose

Change image to `registry.gitlab.com/stackworx-oss/docker/mssql:15.0.4053.23-2`

### Helm Changes

Update the mssql helm chat
```yaml
image:
  repository: registry.gitlab.com/stackworx-oss/docker/mssql
  tag: 15.0.4053.23-2
```

## Common Errors

```bash
/opt/mssql/bin/sqlservr: Error: The system directory [/.system] could not be created.  Errno [13]
```

The docker volumes are still owned by root. The easiest solution is to delete and re-create them.
```
docker-compose down --volumes
```

## References 

- https://hub.docker.com/_/microsoft-mssql-server
- https://github.com/microsoft/mssql-docker/blob/master/linux/preview/examples/mssql-server-linux-non-root/Dockerfile-2019
- https://github.com/microsoft/mssql-docker/blob/master/linux/preview/examples/mssql-agent-fts-ha-tools/Dockerfile