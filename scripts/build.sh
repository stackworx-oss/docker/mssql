#!/bin/bash
set -e

export IMAGE=registry.gitlab.com/stackworx-oss/docker/mssql:15.0.4053.23-2
docker image build -t $IMAGE - < Dockerfile
docker push $IMAGE
